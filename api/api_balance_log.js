const express = require("express");
const router = express.Router();

const balanceLog = require("../models/balance_log_model");

router.post("/createBalanceLog", async(req, res) =>{
    try {
        let { mo_date, mo_no, process, line, child_part_no,
            lotno, operation, qty, balance_qty} = req.body;
        let result = balanceLog.create({
            MO_DATE: mo_date,
            MO_NO: mo_no,
            PROCESS: process,
            LINE: line,
            CHILD_PART_NUMBER: child_part_no,
            LOT_NO: lotno,
            operation : operation,
            QTYS: qty,
            balance_qty: balance_qty
        })
        res.json({ api_result: "OK", message: JSON.stringify(result)});
    } catch (error) {
        return json({result: "Fail", message: error.message})
    }
})

module.exports = router;