const express = require("express");
const router = express.Router();
const rdbms = require("../instance/ms_instance");

router.get("/AllMasterSG", async (req, res) =>{
    try {
        result = await rdbms.query(`
            dbo.SP_GET_ALL_MASTER_SG
        `);
        return res.json({result: result[0], message: "OK"})
    } catch (error) {
        return res.json({
            result: "Failed",
            message: error.message});
    }
})

module.exports = router;