const express = require("express");
const router = express.Router();
const target = require("../models/target_master");

// Target by monthly 

router.post("/add_data", async (req, res) => {
    try {
        const result = await target.create(req.body);
        console.log(result);
        res.json({
            api_result: "OK",
            message: JSON.stringify(result),
        });
    } catch (error) {
        return res.json({
            api_result: "Failed",
            message: error.message,
        });
    }
});
router.post("/select_data", async (req, res) => {
    try {
        const result = await target.findAll();
        console.log(result);
        res.json({
            api_result: "OK",
            result: result,
        });
    } catch (error) {
        return res.json({ error: error })
    }
});

router.post("/cus_PN", async (req, res) => {
    try {
        const result = await target.sequelize.query(
            `SELECT  [CUSTOMER_PN]
            FROM [ushintraceback].[dbo].[master_fg] 
            group by [CUSTOMER_PN]
            `
        )
        console.log(result);
        res.json({
            api_result: "OK",
            message: result[0],
        });
    } catch (error) {
        return res.json({
            api_result: "Failed",
            message: error.message,
        });
    }
});





module.exports = router;