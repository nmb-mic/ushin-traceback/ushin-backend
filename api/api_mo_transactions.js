const express = require("express");
const router = express.Router();
const transactions = require("../models/mo_transaction_model");

const { Op } = require("sequelize");

router.get("/allTransactionsOrder", async(req, res) => {
    try {
        const result = await transactions.findAll({
        });
        return res.json({result: result, message: "OK"})
    } catch (error) {
        return res.json({
            result: "Failed",
            message: error.message});
    }
})

router.post("/partNumberCheck", async(req, res) =>{
    try {
        const { part_no } = req.body;
        const result = await transactions.findAll({
            where: {
                [Op.and] : [
                {
                    CHILD_PART_NUMBER : part_no
                },]
            }  
        })
        return res.json({result: result, message: "OK"})
    } catch (error) {
        return res.json({
            result: "Failed",
            message: error.message});
    }
})

router.post("/insertTransactions", async(req, res) =>{
    try {
        const { mo_no, cus_no, process_code, part_no, part_name, qty_per_box, qty} = req.body;
        const result = await transactions.create({
            MO_NUMBER : mo_no,
            CUSTOMER_NUMBER : cus_no,
            PROCESS_CODE : process_code,
            PROCESS_NAME : "MOCK_UP",
            CHILD_PART_NUMBER : part_no,
            CHILD_PART_NAME : part_name,
            QTY_PER_BOX : qty_per_box,
            QTYS : qty, 
        })
        return res.json({result: result,  message: "OK" })
    } catch (error) {
        return res.json({
            result: "Failed",
            message: error.message});
    }
})

module.exports = router;