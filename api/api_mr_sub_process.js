const express = require("express");
const router = express.Router();
const processPart = require("../models/mr_process_model");

router.post("/insertProcessPart", async (req, res) => {
  try {
    const { process_part } = req.body;
    const result = await processPart.bulkCreate(process_part);
    return res.json({ result: result, message: "OK" });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

router.post("/getProcessLinePart", async (req, res) => {
  try {
    console.log(req.body);
    const result = await processPart.sequelize.query(` with tb1 as (SELECT 
            [CUSTOMER_PART_NUMBER]
            ,[PROCESS_CODE]
            ,[PROCESS_NAME]
           ,[PROCESS_LINE]
           ,[CHILD_PART_NUMBER]
           ,[CHILD_PART_NAME]
           ,[QTY_PER_BOX]
           ,[QTY_PER_SET]
           ,[QTYS]
       FROM [ushintraceback].[dbo].[MR_PROCESS_PART]
       where [PROCESS_LINE] ='${req.body.process_line}'), 
     tb2 as (SELECT [MO_DATE],[MO_NO],[Customers_PN],sum([Due_Qty]) as sumQty
     FROM [ushintraceback].[dbo].[production_sheet]
     where [MO_DATE] ='${req.body.MO_DATE}'
     group by [MO_DATE],[MO_NO],[Customers_PN] ),
     tb3 as (select * from tb1 left join tb2 on tb1.[CUSTOMER_PART_NUMBER] = tb2.[Customers_PN])
     select [CHILD_PART_NUMBER],[CHILD_PART_NAME] ,[PROCESS_NAME] ,[PROCESS_LINE] ,[PROCESS_CODE],[QTY_PER_BOX],[QTY_PER_SET],[QTYS],sum(sumQty) as TTLQty from tb3
     group by [CHILD_PART_NUMBER],[CHILD_PART_NAME] ,[PROCESS_NAME] ,[PROCESS_LINE] ,[PROCESS_CODE],[QTYS],[QTY_PER_SET],[QTY_PER_BOX]`);
    return res.json({ result: result, message: "OK" });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

module.exports = router;
