const express = require("express");
const router = express.Router();

const qrCode = require("qrcode");

router.get('/getQRManufacturingOrder', async(req, res) =>{
    try {
        const qrCodeImg = await qrCode.toDataURL("explate\tdata\tgenerate");
        res.json({ qrCodeImg });
    } catch (error) {
        return res.json({
            result: "Failed",
            message: error.message});
    }
})

module.exports = router;