const express = require("express");
const router = express.Router();
//const rdbms = require("../instance/ms_instance");
const finished_goods = require("../models/fg_model");
const productionSheet = require("../models/production_sheet");

const kitupreceived = require("../models/kitup_part_received");
const kitupissue = require("../models/kitup_part_issue");

router.get("/resultInfo", async (req, res) => {
  try {
    let result = await finished_goods.sequelize.query(`SELECT top 1
    [MO_DATE]
    ,[MO_NO]
   ,[CUSTOMER_PN]
    ,[PROCESS]
    ,[LINE]
  ,[createdAt]
FROM [ushintraceback].[dbo].[record_FGs]
GROUP BY  [MO_DATE]
    ,[MO_NO]
    ,[PROCESS]
    ,[LINE]  ,[CUSTOMER_PN],[createdAt]
  order by [createdAt] desc      `);

    res.json({
      result: result[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

router.get("/resultFG_SG_Linestock", async (req, res) => {
  try {
    let resultMO = await finished_goods.sequelize.query(`SELECT top 1
    [MO_DATE]
    ,[MO_NO]
   ,[CUSTOMER_PN]
    ,[PROCESS]
    ,[LINE]
   , convert(varchar, [createdAt], 31) as newDate
FROM [ushintraceback].[dbo].[record_FGs]
GROUP BY  [MO_DATE]
    ,[MO_NO]
    ,[PROCESS]
    ,[LINE]  ,[CUSTOMER_PN],[createdAt]
  order by [createdAt] desc      `);
    var MO_FG = resultMO[0][0].MO_DATE;
    console.log(MO_FG);

    let resultUnit = await finished_goods.sequelize.query(`SELECT  
    [CUSTOMER_PN]
    ,[QTYS]
FROM [ushintraceback].[dbo].[master_fg]
    `);
    var unitFG = resultUnit[0][0].QTYS;
    console.log(resultUnit[0][0].QTYS);

    let result = await finished_goods.sequelize.query(`with tb1 as (
        SELECT [MO_DATE],
              [LINE]
              ,[CHILD_PART_NUMBER]
              ,sum([QTYS]) as stockQty
          FROM [ushintraceback].[dbo].[record_childParts]
            where [MO_DATE]='${MO_FG}'
          group by [CHILD_PART_NUMBER],[MO_DATE], [LINE]),tb2 as (SELECT 
              [MO_DATE]
              ,[LINE]
              ,[CUSTOMER_PN]
               ,count([CUSTOMER_PN]) as countCustomer_PN
          FROM [ushintraceback].[dbo].[record_FGs]
          group by [MO_DATE],[LINE]
              ,[CUSTOMER_PN])
             SELECT 
            [CHILD_PART_NUMBER]
              ,stockQty
              ,tb2.countCustomer_PN*'${unitFG}' as use_FG
              ,stockQty- tb2.countCustomer_PN*'${unitFG}' as balanceQty
          FROM tb1
           left join tb2 on tb1.[MO_DATE] = tb2.[MO_DATE]
          group by [CHILD_PART_NUMBER],stockQty,tb2.countCustomer_PN,tb2.[CUSTOMER_PN]
          `);

    res.json({
      result: result[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});
router.get("/resultFG_SG_stock", async (req, res) => {
  try {
    let resultUnit = await finished_goods.sequelize.query(`SELECT  
    [CUSTOMER_PN]
    ,[QTYS]
FROM [ushintraceback].[dbo].[master_fg]
    `);
    var unitFG = resultUnit[0][0].QTYS;
    console.log(resultUnit[0][0].QTYS);

    let result = await finished_goods.sequelize.query(`SELECT 
    [MO_DATE]
    ,[MO_NO]
   ,[CUSTOMER_PN]
    ,[PROCESS]
	,count([ORDER_NO])*'${unitFG}' AS QTY
	,[LINE]
  ,convert(varchar, [createdAt], 31) newDate
FROM [ushintraceback].[dbo].[record_FGs]
GROUP BY  [MO_DATE]
    ,[MO_NO]
    ,[PROCESS]
    ,[LINE]  ,[CUSTOMER_PN],convert(varchar, [createdAt], 31) 
  order by convert(varchar, [createdAt], 31) desc       `);

    res.json({
      result: result[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});
router.post("/result_ProductionSheet", async (req, res) => {
  console.log(req.body);
  try {
    let resultMO_Qty = await productionSheet.sequelize.query(`
    with tb1 as (SELECT 
      [CUSTOMER_PART_NUMBER]
     ,[PROCESS_LINE]
     ,[CHILD_PART_NUMBER]
     ,[CHILD_PART_NAME]
     ,[QTY_PER_BOX]
     ,[QTY_PER_SET]
     ,[QTYS]
 FROM [ushintraceback].[dbo].[MR_PROCESS_PART]
 where [PROCESS_LINE] ='${req.body.process_line}')
SELECT [MO_DATE],[MO_NO],sum([Due_Qty]) as TTLQty
FROM [ushintraceback].[dbo].[production_sheet]
where [MO_DATE]='${req.body.MO_DATE}'
group by [MO_DATE],[MO_NO]
    `);
    console.log("TEST", resultMO_Qty);
    res.json({
      result: resultMO_Qty[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

router.get("/resultMaterial_Kit_Received", async (req, res) => {
  try {
    let result = await kitupreceived.sequelize.query(`SELECT TOP (20) [id]
    ,[MO_DATE]
    ,[MO_NO]
    ,[PROCESS]
    ,[LINE]
    ,[CHILD_PART_NUMBER]
    ,[LOT_NO]
    ,[QTYS]
    ,[createdAt]
    ,[updatedAt]
FROM [ushintraceback].[dbo].[record_kitupReceived]
ORDER BY [createdAt] desc    `);
    console.log(result[0]);
    res.json({
      result: result[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

router.get("/resultMaterial_Kit_Issue", async (req, res) => {
  try {
    let result = await kitupissue.sequelize.query(`SELECT TOP (20) [id]
    ,[MO_DATE]
    ,[MO_NO]
    ,[PROCESS]
    ,[LINE]
    ,[CHILD_PART_NUMBER]
    ,[LOT_NO]
    ,[QTYS]
    ,[createdAt]
    ,[updatedAt]
    FROM [ushintraceback].[dbo].[record_kitupIssue]
ORDER BY [createdAt] desc    `);
    console.log(result[0]);
    res.json({
      result: result[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

router.get("/resultMaterial_KitupStock", async (req, res) => {
  console.log(req.body);

  try {
    let resultMat_Qty = await kitupreceived.sequelize.query(`with tb1 as(select 
      [PROCESS]
      ,[LINE]
      ,[CHILD_PART_NUMBER]
      ,sum([QTYS]) as tb1Qty
  FROM [ushintraceback].[dbo].[record_kitupReceived]
   group by [CHILD_PART_NUMBER],[PROCESS],[LINE]
  ),tb2 as (select 
      [PROCESS]
      ,[LINE]
      ,[CHILD_PART_NUMBER]
      ,sum([QTYS]) as tb2Qty
  FROM [ushintraceback].[dbo].[record_kitupIssue]
  group by [CHILD_PART_NUMBER],[PROCESS],[LINE]),
 tb3 as (
  select
      tb1.[PROCESS]
      ,tb1.[LINE]
      ,tb1.[CHILD_PART_NUMBER]
      , tb1.tb1Qty as tb1_newQty
	  ,isnull(tb2.tb2Qty,0) as tb2_newQty
    FROM tb1
          left join tb2 on tb1.[CHILD_PART_NUMBER] = tb2.[CHILD_PART_NUMBER]
		  )
	select  [PROCESS]
      ,[LINE]
      ,[CHILD_PART_NUMBER]
	  ,tb1_newQty-tb2_newQty as balanceStock
	  from tb3  `);
    console.log(resultMat_Qty);
    res.json({
      result: resultMat_Qty[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});
router.get("/resultMaterial_Linestock", async (req, res) => {
  console.log(req.body);

  try {
    let resultUnit = await finished_goods.sequelize.query(`SELECT  
    [CUSTOMER_PN]
    ,[QTYS]
FROM [ushintraceback].[dbo].[master_fg]
    `);
    var unitFG = resultUnit[0][0].QTYS;
    console.log(resultUnit[0][0].QTYS);

    let resultMat_Qty = await finished_goods.sequelize.query(`with tb1 as (
      SELECT 
            [CHILD_PART_NUMBER]
            ,[CHILD_PART_NAME]
            ,[QTY_PER_BOX]
            ,[QTY_PER_SET]
            ,[createdAt]
        FROM [ushintraceback].[dbo].[MR_PROCESS_PART]),
        tb2 as ( SELECT
       [createdAt]
            ,[MO_DATE]
            ,[MO_NO]
            ,[PROCESS]
            ,[LINE]
            ,[CHILD_PART_NUMBER]
      ,[LOT_NO]
        FROM [ushintraceback].[dbo].[record_kitupIssue]
        group by [CHILD_PART_NUMBER],[createdAt]
            ,[MO_DATE]
            ,[MO_NO]
            ,[PROCESS]
            ,[LINE]
          ,[createdAt],[LOT_NO]),
          tb3 as (
        SELECT 
		tb2.[PROCESS] as newProcess
        ,tb2.[createdAt] as newCreateAt
       ,tb2.[MO_DATE] as newMO_DATE
           ,tb2.[MO_NO] as newMO_NO
           ,tb2.[LINE] as newLINE
           ,tb1.[CHILD_PART_NUMBER] as newCHILD
         ,tb2.[LOT_NO] as newLOT_NO
         ,tb1.[QTY_PER_SET] as newQty_set
          FROM tb2
          left join tb1 on tb1.[CHILD_PART_NUMBER] = tb2.[CHILD_PART_NUMBER]),
        tb4 as (
        SELECT newProcess,newMO_DATE,newLINE,newCHILD ,sum(newQty_set) as sumQTY FROM tb3
        group by newProcess,newCHILD,newQty_set,newLINE,newMO_DATE),
        tb5 as (
      SELECT 
      [MO_DATE]
      ,[LINE]
      ,[CUSTOMER_PN]
      ,count([ORDER_NO])*'${unitFG}' as FGQty
      FROM [ushintraceback].[dbo].[record_FGs]
      group by [MO_DATE],[LINE],[CUSTOMER_PN])
      SELECT newProcess,newMO_DATE,newLINE,newCHILD ,sumQTY - tb5.FGQty as balanceQty  FROM tb4
       left join tb5 on tb4.newMO_DATE = tb5.[MO_DATE]    `);
    console.log(resultMat_Qty);
    res.json({
      result: resultMat_Qty[0],
      api_result: "OK",
      // message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      result: "Failed",
      message: error.message,
    });
  }
});

module.exports = router;
