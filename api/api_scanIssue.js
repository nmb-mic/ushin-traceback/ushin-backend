const express = require("express");
const router = express.Router();

const issue = require("../models/kitup_model");
const finished_goods = require("../models/fg_model");

router.post("/scanInsert_material", async (req, res) => {
  try {
    let {
      id,
      mo_date,
      mo_no,
      process,
      line,
      child_part_no,
      lotno,
      qty,
      createdAt,
      updatedAt,
    } = req.body;

    console.log("test", req.body);

    let result = await issue.create({
      id: id,
      MO_DATE: mo_date,
      MO_NO: mo_no,
      PROCESS: process,
      LINE: line,
      CHILD_PART_NUMBER: child_part_no,
      LOT_NO: lotno,
      QTYS: qty,
      createdAt: createdAt,
      updatedAt: updatedAt,
    });
    res.json({
      api_result: "OK",
      message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      api_result: "Failed",
      message: error.message,
    });
  }
});

router.post("/scanInsert_FG", async (req, res) => {
  try {
    let {
      id,
      mo_date,
      mo_no,
      process,
      line,
      customer_PN,
      DO_number,
      card_no,
      due_date,
      order_no,
      createdAt,
      updatedAt,
    } = req.body;

    console.log("test", req.body);

    let result = await finished_goods.create({
      id: id,
      MO_DATE: mo_date,
      MO_NO: mo_no,
      PROCESS: process,
      LINE: line,
      CUSTOMER_PN: customer_PN,
      DO_NUMBER: DO_number,
      CARD_NO: card_no,
      DUE_DATE: due_date,
      ORDER_NO: order_no,
      createdAt: createdAt,
      updatedAt: updatedAt,
    });
    res.json({
      api_result: "OK",
      message: JSON.stringify(result),
    });
  } catch (error) {
    return res.json({
      api_result: "Failed",
      message: error.message,
    });
  }
});

module.exports = router;