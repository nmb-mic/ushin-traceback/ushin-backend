const express = require("express");
const router = express.Router();

const kitupissue = require("../models/kitup_part_issue");
router.post("/scanInsert_kitupsIssue", async (req, res) => {
    try {
      let {
        id,
        mo_date,
        mo_no,
        process,
        line,
        child_part_no,
        lotno,
        qty,
        createdAt,
        updatedAt,
      } = req.body;
  
      console.log("test", req.body);
  
      let result = await kitupissue.create({
        id: id,
        MO_DATE: mo_date,
        MO_NO: mo_no,
        PROCESS: process,
        LINE: line,
        CHILD_PART_NUMBER: child_part_no,
        LOT_NO: lotno,
        QTYS: qty,
        createdAt: createdAt,
        updatedAt: updatedAt,
      });
      res.json({
        api_result: "OK",
        message: JSON.stringify(result),
      });
    } catch (error) {
      return res.json({
        api_result: "Failed",
        message: error.message,
      });
    }
  });
module.exports = router;