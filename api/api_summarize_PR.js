const express = require("express");
const router = express.Router();
const issue = require("../models/kitup_model");
const finished_goods = require("../models/fg_model");


router.post("/output_hr", async (req, res) => {
  let result = await finished_goods.sequelize.query(
    `
      --get summarize output by hr 
      with joinTb as (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE],[record_FGs].[PROCESS],[record_FGs].[LINE]
            ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[record_FGs].[createdAt],[master_fg].[QTYS]
            ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]
            ,IIF(CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)=0,23,CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)) as [hour]
        FROM [record_FGs] left join [master_fg]
        on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN
        )
        select [CUSTOMER_PN], [PROCESS],[LINE],[mfgdate],[hour]
        ,sum([QTYS]) as [QTYS]
        from joinTb 
        where [mfgdate] = '${req.body.mfg_date}'
        group by [CUSTOMER_PN], [PROCESS],[LINE],[mfgdate],[hour]
        `
  );
  console.log("*********** Output by hr *************", result);
  res.json({ result: result[0] });
});
router.post("/output_daily", async (req, res) => {
  let result = await finished_goods.sequelize.query(
    ` with joinTb as (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE],[record_FGs].[PROCESS],[record_FGs].[LINE]			
      ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[record_FGs].[createdAt],[master_fg].[QTYS]		
      ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]		
      ,IIF(CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)=0,23,CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)) as [hour]		
    FROM [record_FGs] left join [master_fg]			
    on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN			
    )			
    select [CUSTOMER_PN], [PROCESS],[LINE],[mfgdate]		
    ,sum([QTYS]) as [QTYS]			
    from joinTb 			
    --where [mfgdate] = '2023-07-16' 			
    group by [CUSTOMER_PN], [PROCESS],[LINE],[mfgdate]	
        `
  );
  console.log("*********** Output by daily *************", result);
  res.json({ result: result[0] });
});
router.post("/daily_stack", async (req, res) => {
  try {
    let qry_line = ``;
    if (req.body.line == "All" && req.body.Customer == "All") {
      qry_line = ``;
    } else {
      qry_line = `where [record_FGs].[LINE] = '${req.body.line}' and [record_FGs].[CUSTOMER_PN] = '${req.body.Customer}'`;
    } if (req.body.line == "All" && req.body.Customer !== "All") {
      qry_line = `where [record_FGs].[CUSTOMER_PN] = '${req.body.Customer}'`;
    } if ((req.body.line !== "All" && req.body.Customer == "All")) {
      qry_line = `where  [record_FGs].[LINE]  = '${req.body.line}'`;
    }
    let result = await finished_goods.sequelize.query(
      ` 
      with joinTb as (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE],[record_FGs].[PROCESS],[record_FGs].[LINE]			
        ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[record_FGs].[createdAt],[master_fg].[QTYS]		
        ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]		
        ,IIF(CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)=0,23,CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)) as [hour]		
        ,200 as [target] 
	    FROM [record_FGs] left join [master_fg]			
       on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN	`
      + qry_line + ` 		
       )			
     , tb1 as (  select [LINE],[CUSTOMER_PN], [mfgdate],[target], ISNULL([QTYS],0) as [QTYS]
       from joinTb
     ) 
     ,TB_PVT AS (select [mfgdate],[target],ISNULL([69005EW021],0) as [PN_69005EW021],ISNULL([69005EW031],0) as [PN_69005EW031] ,ISNULL([77310EW041],0) as [PN_77310EW041] 
	 from tb1
     PIVOT (sum([QTYS]) 
     for [CUSTOMER_PN] in ([69005EW021],[69005EW031],[77310EW041]) 
       ) as pvt
     )
    select *  from TB_PVT
          `
    );
    console.log("*********** Output by daily C/N  *************", result);
    res.json({ result: result[0] });

  } catch (error) {
    res.json({ error: error })
  }

});
router.post("/hour_stack", async (req, res) => {
  try {
    let qry_line = ``;
    if (req.body.line == "All" && req.body.Customer == "All") {
      qry_line = ``;
    } else {
      qry_line = `and [record_FGs].[LINE] = '${req.body.line}' and [record_FGs].[CUSTOMER_PN] = '${req.body.Customer}'`;
    } if (req.body.line == "All" && req.body.Customer !== "All") {
      qry_line = `and [record_FGs].[CUSTOMER_PN] = '${req.body.Customer}'`;
    } if ((req.body.line !== "All" && req.body.Customer == "All")) {
      qry_line = `and  [record_FGs].[LINE]  = '${req.body.line}'`;
    }
    let result = await finished_goods.sequelize.query(
      ` 
      with tb1 as  (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE],[record_FGs].[PROCESS],[record_FGs].[LINE]			
        ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[master_fg].[QTYS]		
        ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]	
        ,50 as [Target]	
        ,IIF(CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)=0,0,CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)) as [hour]		
     FROM [record_FGs] left join [master_fg]			
       on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN	
       where convert(varchar, [record_FGs].[createdAt], 23) = '${req.body.mfg_date}'  
       ` + qry_line + ` )
 ,tb2 as (select CASE
                        WHEN [hour] = 8 THEN 1
                        WHEN [hour] = 9 THEN 2
                        WHEN [hour] = 10 THEN 3
                        WHEN [hour] = 11 THEN 4
                        WHEN [hour] = 12 THEN 5
                        WHEN [hour] = 13 THEN 6
                        WHEN [hour] = 14 THEN 7
                        WHEN [hour] = 15 THEN 8
                        WHEN [hour] = 16 THEN 9
                        WHEN [hour] = 17 THEN 10
                        WHEN [hour] = 18 THEN 11
                        WHEN [hour] = 19 THEN 12
                        WHEN [hour] = 20 THEN 13
                        WHEN [hour] = 21 THEN 14
                        WHEN [hour] = 22 THEN 15
                        WHEN [hour] = 23 THEN 16
                        WHEN [hour] = 0 THEN 17
                        WHEN [hour] = 1 THEN 18
                        WHEN [hour] = 2 THEN 19
                        WHEN [hour] = 3 THEN 20
                        WHEN [hour] = 4 THEN 21
                        WHEN [hour] = 5 THEN 22
                        WHEN [hour] = 6 THEN 23
                       WHEN [hour] = 7 THEN 24
                    else 0 end as [row],[hour],[CUSTOMER_PN],[QTYS] 
 from tb1) 
   ,TB_PVT AS (select [hour],Target,ISNULL([69005EW021],0) as [PN_69005EW021],ISNULL([69005EW031],0) as [PN_69005EW031] ,ISNULL([77310EW041],0) as [PN_77310EW041] 
  from tb1
    PIVOT (sum([QTYS]) 
    for [CUSTOMER_PN] in ([69005EW021],[69005EW031],[77310EW041]) 
      ) as pvt
    )  select [hour],Target,sum([PN_69005EW021]) as [PN_69005EW021]
  ,sum([PN_69005EW031])as[PN_69005EW031],sum ([PN_77310EW041]) as [PN_77310EW041]  from TB_PVT 
  group by [hour],[Target] order by [hour]
          `
    );
    console.log("*********** Output by hour stack  *************", result);
    res.json({ result: result[0] });

  } catch (error) {
    res.json({ error: error })
  }

});
router.post("/hour_data", async (req, res) => {
  try {
    let qry_line = ``;
    if (req.body.line == "All" && req.body.Customer == "All") {
      qry_line = ``;
    } else {
      qry_line = `and [record_FGs].[LINE] = '${req.body.line}' and [record_FGs].[CUSTOMER_PN] = '${req.body.Customer}'`;
    } if (req.body.line == "All" && req.body.Customer !== "All") {
      qry_line = `and [record_FGs].[CUSTOMER_PN] = '${req.body.Customer}'`;
    } if ((req.body.line !== "All" && req.body.Customer == "All")) {
      qry_line = `and  [record_FGs].[LINE]  = '${req.body.line}'`;
    }
    let result = await finished_goods.sequelize.query(
      ` 
      with tb1 as  (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE],[record_FGs].[PROCESS],[record_FGs].[LINE]
        ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[master_fg].[QTYS]
        ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]
        ,10 as [Target]
        ,IIF(CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)=0,0,CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)) as [hour]
     FROM [record_FGs] left join [master_fg]
       on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN
       where convert(varchar, [record_FGs].[createdAt], 23) = '${req.body.mfg_date}'  
       ` + qry_line + ` )
 ,tb2 as (select CASE
                        WHEN [hour] = 8 THEN 1
                        WHEN [hour] = 9 THEN 2
                        WHEN [hour] = 10 THEN 3
                        WHEN [hour] = 11 THEN 4
                        WHEN [hour] = 12 THEN 5
                        WHEN [hour] = 13 THEN 6
                        WHEN [hour] = 14 THEN 7
                        WHEN [hour] = 15 THEN 8
                        WHEN [hour] = 16 THEN 9
                        WHEN [hour] = 17 THEN 10
                        WHEN [hour] = 18 THEN 11
                        WHEN [hour] = 19 THEN 12
                        WHEN [hour] = 20 THEN 13
                        WHEN [hour] = 21 THEN 14
                        WHEN [hour] = 22 THEN 15
                        WHEN [hour] = 23 THEN 16
                        WHEN [hour] = 0 THEN 17
                        WHEN [hour] = 1 THEN 18
                        WHEN [hour] = 2 THEN 19
                        WHEN [hour] = 3 THEN 20
                        WHEN [hour] = 4 THEN 21
                        WHEN [hour] = 5 THEN 22
                        WHEN [hour] = 6 THEN 23
                       WHEN [hour] = 7 THEN 24
                    else 0 end as [row],[mfgdate],[CUSTOMER_PN],[CUSTOMER_NAME],[MO_DATE],[DO_NUMBER],[ORDER_NO],[QTYS]
 from tb1 )  select  * from tb2
          `
    );
    console.log("*********** Output by hour stack  *************", result);
    res.json({ result: result[0] });

  } catch (error) {
    res.json({ error: error })
  }

});
router.post("/Daily_data", async (req, res) => {
  try {
    let qry_line = ``;
    if (req.body.line == "All" && req.body.Customer == "All") {
      qry_line = ``;
    } else {
      qry_line = `where LINE = '${req.body.line}' and CUSTOMER_PN = '${req.body.Customer}'`;
    } if (req.body.line == "All" && req.body.Customer !== "All") {
      qry_line = `where CUSTOMER_PN = '${req.body.Customer}'`;
    } if ((req.body.line !== "All" && req.body.Customer == "All")) {
      qry_line = `where LINE = '${req.body.line}'`;
    }
    let result = await finished_goods.sequelize.query(
      ` 
      with joinTb as (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE],[record_FGs].[PROCESS],[record_FGs].[LINE]
        ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[record_FGs].[createdAt],[master_fg].[QTYS]
        ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]
        ,IIF(CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)=0,23,CAST(DATEPART(HOUR, [record_FGs].[createdAt]) AS int)) as [hour]
        FROM [record_FGs] left join [master_fg]
        on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN
        )
        select mfgdate, CUSTOMER_PN,CUSTOMER_NAME,MO_DATE,LINE,DO_NUMBER,ORDER_NO,QTYS  from joinTb
        `
      + qry_line + ` 		
          `
    );
    console.log("*********** Output by daily *************", result);
    res.json({ result: result[0] });

  } catch (error) {
    res.json({ error: error })
  }

});
router.post("/byMonth", async (req, res) => {
  try {
    let result = await finished_goods.sequelize.query(
      ` with joinTb as (SELECT [master_fg].[CUSTOMER_PN],[master_fg].[CUSTOMER_NAME] ,[record_FGs].[MO_DATE]
        ,[record_FGs].[PROCESS],[record_FGs].[LINE]
        ,[record_FGs].[DO_NUMBER],[record_FGs].[ORDER_NO] ,[record_FGs].[createdAt],[master_fg].[QTYS]
        ,convert(varchar, [record_FGs].[createdAt], 23) as [mfgdate]
        ,FORMAT([createdAt], 'yyyy-MM') as [month]
        FROM [record_FGs] left join [master_fg]
        on [record_FGs].CUSTOMER_PN = [master_fg].CUSTOMER_PN
        )
        ,TB_PVT as (select [month],ISNULL([69005EW021],0) as [PN_69005EW021],ISNULL([69005EW031],0) as [PN_69005EW031] 
        ,ISNULL([77310EW041],0) as [PN_77310EW041]  from joinTb  
           PIVOT (sum([QTYS]) 
             for [CUSTOMER_PN] in ([69005EW021],[69005EW031],[77310EW041]) 
           ) as pvt
        )    select [month],sum([PN_69005EW021]) as PN_69005EW021 ,sum([PN_69005EW031]) as PN_69005EW031 ,sum([PN_77310EW041]) as PN_77310EW041 from TB_PVT 
        group by [month]
          `
    );
    console.log("*********** Output by Month  *************", result);
    res.json({ result: result[0] });

  } catch (error) {
    res.json({ error: error })
  }

});



module.exports = router;