const Sequelize = require("sequelize");
const sequelize = require("../instance/ms_instance");

const finished_goods = sequelize.define("record_FGs", {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    autoIncrement: true,
  },
  MO_DATE: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },

  MO_NO: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  PROCESS: {
    type: Sequelize.STRING(50),
    allowNull: true,
  },
  LINE: {
    type: Sequelize.STRING(50),
    allowNull: true,
  },

  CUSTOMER_PN: {
    type: Sequelize.STRING(50),
    allowNull: false,

  },

  DO_NUMBER: {
    type: Sequelize.STRING(50),
    primaryKey: true,
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  CARD_NO: {
    type: Sequelize.STRING(50),

    allowNull: true,
 
  },
  DUE_DATE: {
    type: Sequelize.STRING(50),
    allowNull: true,
  },

  ORDER_NO: {
    type: Sequelize.STRING(50),
    primaryKey: true,
    allowNull: true,
   
  },

});

(async () => {
  await finished_goods.sync({ force: false });
})();

module.exports = finished_goods;
