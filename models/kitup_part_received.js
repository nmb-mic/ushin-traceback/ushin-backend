const Sequelize = require("sequelize");
const sequelize = require("../instance/ms_instance");

const kitupreceived = sequelize.define("record_kitupReceived", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  MO_DATE: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },

  MO_NO: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  PROCESS: {
    type: Sequelize.STRING(50),
    allowNull: true,
  },
  LINE: {
    type: Sequelize.STRING(50),
    allowNull: true,
  },
  CHILD_PART_NUMBER: {
    type: Sequelize.STRING(50),
    allowNull: false,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },

  LOT_NO: {
    type: Sequelize.STRING(50),
    primaryKey: true,
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },

  QTYS: {
    type: Sequelize.INTEGER,
    allowNull: true,
  },
},
{
  freezeTableName : true
});

(async () => {
  await kitupreceived.sync({ force: false });
})();

module.exports = kitupreceived;
