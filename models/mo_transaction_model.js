const Sequelize = require("sequelize");
const sequelize = require("../instance/ms_instance");

const transactions = sequelize.define(
  "MO_TRANSACTION",
  {
    TRANSACTION_ID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
    MO_NUMBER: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "REQUIRED",
        },
      },
    },
    CUSTOMER_NUMBER: {
        type: Sequelize.STRING(16),
        allowNull: false,
        validate: {
          notEmpty: {
            args: true,
            msg: "REQUIRED",
          },
        },
      },
    PROCESS_CODE: {
        type: Sequelize.STRING(3),
        allowNull: false,
        validate: {
            notEmpty: {
            args: true,
            msg: "REQUIRED",
            },
        },
    },
    PROCESS_NAME: {
        type: Sequelize.STRING(50),
        allowNull: false,
        validate: {
            notEmpty: {
            args: true,
            msg: "REQUIRED",
            },
        },
    },
    CHILD_PART_NUMBER: {
        type: Sequelize.STRING(16),
        allowNull: false,
        validate: {
            notEmpty: {
            args: true,
            msg: "REQUIRED",
            },
        },
    },
    CHILD_PART_NAME: {
        type: Sequelize.STRING(50),
        allowNull: false,
        validate: {
            notEmpty: {
            args: true,
            msg: "REQUIRED",
            },
        },
    },
    QTY_PER_BOX: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    QTYS: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
  },
  {
    //options
    freezeTableName: true,
  }
);

(async  ()=>{
    await transactions.sync({ force: true});
})();

module.exports = transactions;