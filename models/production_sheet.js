const Sequelize = require("sequelize");
const sequelize = require("../instance/ms_instance");

const productionSheet = sequelize.define("production_sheet", {

    PO_NO: {
    type: Sequelize.STRING(50),
    primaryKey: true,
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },


  Supp: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Supp_Name: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Supp: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },

  CMSP: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Part_Number: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Prcs: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  P_Name: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Due_Date: {
    type: Sequelize.DATE,
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Due_Qty: {
    type: Sequelize.INTEGER,
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Prdt_Qty: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },

  Balance: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Unit: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  OD_Days: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  DO_Number: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  CardNo: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Total: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Delivery_Due: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  Customers_PN: {
    type: Sequelize.DATE,
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  MO_DATE: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },
  MO_NO: {
    type: Sequelize.STRING(50),
    allowNull: true,
    validate: {
      notEmpty: {
        args: true,
        msg: "REQUIRED",
      },
    },
  },









});

(async () => {
  await productionSheet.sync({ force: false });
})();

module.exports = productionSheet;
