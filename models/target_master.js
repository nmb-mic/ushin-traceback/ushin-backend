const Sequelize = require("sequelize");
const sequelize = require("../instance/ms_instance");

const target = sequelize.define("master_target", {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    Customer_PN: {
        type: Sequelize.STRING(50),
        allowNull: true,
        validate: {
            notEmpty: {
                args: true,
                msg: "REQUIRED",
            },
        },
    },
    PROCESS: {
        type: Sequelize.STRING(50),
        allowNull: false,
    },

    Target: {
        type: Sequelize.INTEGER,
        allowNull: false,
    }, 
    updateBy: {
        type: Sequelize.STRING(50),
        allowNull: false,
        validate: {
            notEmpty: {
                args: true,
                msg: "REQUIRED",
            },
        },
    },
});

(async () => {
    await target.sync({ force: false });
})();

module.exports = target;
