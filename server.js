const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "./filclses")));
app.use(cors());

app.use(express.json());

app.use("/api/authen", require("./api/api_login"));
app.use("/api/issue", require("./api/api_scanIssue"));
app.use("/api/finished_goods", require("./api/api_scanIssue"));
app.use("/api/finished_goods", require("./api/api_result"));
app.use("/api/productionSheet", require("./api/api_result"));
app.use("/api/kitupreceived", require("./api/api_result"));
app.use("/api/kitupissue", require("./api/api_result"));
app.use("/api/summarize_PR", require("./api/api_summarize_PR"));
app.use("/api/master_target", require("./api/api_master_target"));

 app.use("/api/single_part", require("./api/api_master_sg"));
 app.use("/api/transaction", require("./api/api_mo_transactions"));
 app.use("/api/processPart", require("./api/api_qr_gen"));
 app.use("/api/mr_process", require("./api/api_mr_sub_process"));
 app.use("/api/balance_log", require("./api/api_balance_log"));
 app.use("/api/KitupReceived", require("./api/api_scanKitup_received"));
 app.use("/api/KitupIssue", require("./api/api_scanKitup_issue"));

app.listen(5010, () => {
 
  console.log("server is up and listening on port 5010");
});

////// Set powershell runscript once before using Nodemon
////---- Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass ----////